package main

import (
	"net/http"

	"todo/handlers"
)

func main() {
	http.Handle("/", handlers.DecorateRequest(handlers.GetTasks))
	http.HandleFunc("/getTodos", handlers.GetTasks)
	http.HandleFunc("/markTodo", handlers.MarkTodo)
	http.HandleFunc("/addTodo", handlers.AddTodo)
	http.ListenAndServe(":8081", nil)
}
